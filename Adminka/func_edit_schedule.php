<?php
include 'config.php';

$group = $_POST['group'];
$semester = $_POST['semester'];
$date = $_POST['date '];
$number_pairs = $_POST['number_pairs'];
$name_pairs = $_POST['name_pairs'];
$auditore = $_POST['auditore'];
$teacher = $_POST['teacher'];

// Create

if (isset($_POST['submit'])) {
	$password = htmlspecialchars($password, ENT_QUOTES);
	$sql = ("INSERT INTO `schedule`(`group`, `semester`, `date`, `number_pairs`, `name_pairs`, `auditore`, `teacher` ) VALUES(?,?,?,?,?,?,?)");
	$query = $pdo->prepare($sql);
	$query->execute([$group, $semester, $date, $number_pairs ,$name_pairs, $auditore, $teacher]);
	$success = '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Данные успешно отправлены!</strong> Вы можете закрыть это сообщение.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
	
}

// Read

$sql = $pdo->prepare("SELECT * FROM `schedule`");
$sql->execute();
$result = $sql->fetchAll();

// Update
$edit_group = $_POST['edit_group'];
$edit_semester = $_POST['edit_last_name'];
$edit_date = $_POST['edit_date'];
$edit_number_pairs = $_POST['edit_number_pairs'];
$edit_name_pairs = $_POST['edit_name_pairs'];
$edit_auditore = $_POST['edit_auditore'];
$edit_teacher = $_POST['edit_teacher'];
$get_id = $_GET['id'];
if (isset($_POST['edit-submit'])) {
	$sqll = "UPDATE schedule SET group=?, semseter=?, date=?, number_pairs=?, name_pairs=?, auditore=?, teacher=? WHERE id=?";
	$querys = $pdo->prepare($sqll);
	$querys->execute([$edit_group, $edit_semester, $edit_date, $edit_number_pairs , $edit_name_pairs , $edit_auditore , $edit_teacher ,$get_id]);
	header('Location: '. $_SERVER['HTTP_REFERER']);
}

// DELETE
if (isset($_POST['delete_submit'])) {
	$sql = "DELETE FROM users WHERE id=?";
	$query = $pdo->prepare($sql);
	$query->execute([$get_id]);
	header('Location: '. $_SERVER['HTTP_REFERER']);
}
