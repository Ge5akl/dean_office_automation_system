<?php
//Запускаем сессию
session_start();
?>
<?php
include 'func.php';
//Подключение шапки
require_once("testHeader.php");
?>
<link rel="stylesheet" href="assets/css/style.css">
<?php
include("dbconnect.php")
?>

<?php
$name = '2020-06-08';
$name = $_POST['date_edit'];
?>
<table class="table">
  <thead>
    <tr>
      <th colspan="5">
        <?php
        $cur_day = $name;
        echo $cur_day; ?>
      </th>
    </tr>
    <tr>
      <th>#</th>
      <th>Название предмета</th>
      <th>Дата</th>
      <th>Аудитория</th>
      <th>Преподаватель</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $email = $_SESSION['email'];
    /* создаем подготавливаемый запрос */
    if ($stmt = $mysqli->prepare("SELECT `group` FROM `users` WHERE email=?")) {

      /* связываем параметры с метками */
      $stmt->bind_param("s", $email);

      /* запускаем запрос */
      $stmt->execute();

      /* связываем переменные с результатами запроса */
      $stmt->bind_result($groupCurr);

      /* получаем значения */
      $stmt->fetch();


      /* закрываем запрос */
      $stmt->close();
    }


    $result = mysqli_query($mysqli, "SELECT DISTINCT schedule.* FROM `users` INNER JOIN `schedule` ON users.group = schedule.group WHERE users.`group` = '$groupCurr' AND schedule.`date` = '$cur_day'");
    foreach ($result as $value) { ?>

      <tr>
        <td><?= $value['number_pairs'] ?></td>
        <td><?= $value['name_pairs'] ?></td>
        <td><?= $value['date'] ?></td>
        <td><?= $value['auditore'] ?></td>
        <td><?= $value['teacher'] ?></td>
        <td>
      </tr>
    <?php
    } ?>
  </tbody>
</table>
<form method="post" class="estimation_view">
  <input type="date" name="date_edit" value="2020-06-10" />
  <button class="btn btn-primary" type="submit">Вывести расписание</button>
</form>