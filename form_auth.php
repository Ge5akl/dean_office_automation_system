
   <!-- Font Icon -->
   <link rel="stylesheet" href="fonts\material-icon\css\material-design-iconic-font.min.css">

   <!-- Main css -->
   <link rel="stylesheet" href="css\Register-style.css">
   <!-- Блок для вывода сообщений -->
   <div class="block_for_messages">
       <?php

        if (isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])) {
            echo $_SESSION["error_messages"];

            //Уничтожаем чтобы не появилось заново при обновлении страницы
            unset($_SESSION["error_messages"]);
        }

        if (isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])) {
            echo $_SESSION["success_messages"];

            //Уничтожаем чтобы не появилось заново при обновлении страницы
            unset($_SESSION["success_messages"]);
        }
        ?>
   </div>

   <?php
    //Проверяем, если пользователь не авторизован, то выводим форму авторизации, 
    //иначе выводим сообщение о том, что он уже авторизован
    if (!isset($_SESSION["email"]) && !isset($_SESSION["password"])) {
    ?>


       <section class="sign-in">
           <div class="container">
               <div class="signin-content">
                   <div class="signin-image">
                       <figure><img src="images/signin-image.jpg" alt="sing up image"></figure>
                       <a href="form_register.php" class="signup-image-link">Создать аккаунт</a>
                   </div>

                   <div class="signin-form" id="form_auth">
                       <h2 class="form-title">Авторизация</h2>
                       <form action="auth.php" method="post" name="form_auth" class="register-form" id="login-form">
                           <div class="form-group">
                               <label for="your_email"><i class="zmdi zmdi-email"></i></label>
                               <input type="email" name="email" required="required" id="your_email" placeholder="Почта" />
                               <span id="valid_email_message" class="mesage_error"></span>
                           </div>
                           <div class="form-group">
                               <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                               <input type="password" type="password" name="password" id="your_pass" placeholder="Password" />
                               <span id="valid_password_message" class="mesage_error"></span>
                           </div>
                           <div class="form-group">
                               <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                               <label for="remember-me" class="label-agree-term"><span><span></span></span>Запомнить меня</label>
                           </div>
                           <div class="form-group form-button">
                               <input type="submit" name="btn_submit_auth" value="Войти" id="signin" class="form-submit" />
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </section>




   <?php
    } else {
    ?>


   <?php
    }
    ?>
   <script src="vendor/jquery/jquery.min.js"></script>
   <script src="js/main.js"></script>