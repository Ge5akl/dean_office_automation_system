  <!-- Font Icon -->
  <link rel="stylesheet" href="fonts\material-icon\css\material-design-iconic-font.min.css">

  <!-- Main css -->
  <link rel="stylesheet" href="css\Register-style.css">
  <!-- Блок для вывода сообщений -->
  <div class="block_for_messages">
      <?php
        //Если в сессии существуют сообщения об ошибках, то выводим их
        if (isset($_SESSION["error_messages"]) && !empty($_SESSION["error_messages"])) {
            echo $_SESSION["error_messages"];

            //Уничтожаем чтобы не выводились заново при обновлении страницы
            unset($_SESSION["error_messages"]);
        }

        //Если в сессии существуют радостные сообщения, то выводим их
        if (isset($_SESSION["success_messages"]) && !empty($_SESSION["success_messages"])) {
            echo $_SESSION["success_messages"];

            //Уничтожаем чтобы не выводились заново при обновлении страницы
            unset($_SESSION["success_messages"]);
        }
        ?>
  </div>

  <?php
    //Проверяем, если пользователь не авторизован, то выводим форму регистрации, 
    //иначе выводим сообщение о том, что он уже зарегистрирован
    if (!isset($_SESSION["email"]) && !isset($_SESSION["password"])) {
    ?>
      <section class="signup">
          <div class="container">
              <div class="signup-content">
                  <div class="signup-form">
                      <h2 class="form-title">Регистрация</h2>
                      <form action="register.php" method="post" name="form_register" class="register-form" id="register-form">
                          <div class="form-group">
                              <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                              <input type="text" name="first_name" id="name" placeholder="Ваше Имя" required="required" />
                          </div>
                          <div class="form-group">
                              <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                              <input type="text" name="last_name" id="fimale" placeholder="Ваша Фамилия" required="required" />
                          </div>
                          <div class="form-group">
                              <label for="email"><i class="zmdi zmdi-email"></i></label>
                              <input type="email" name="email" id="email" placeholder="Ваша почта" required="required" />
                          </div>
                          <div class="form-group">
                              <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                              <input type="password" name="password" id="pass" placeholder="Password" required="required" />
                          </div>
                          <div class="form-group">
                              <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                              <input type="password" name="re_pass" id="re_pass" placeholder="Repeat your password" />
                          </div>
                          <div class="form-group">
                              <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                              <label for="agree-term" class="label-agree-term"><span><span></span></span>Я примимаю <a href="#" class="term-service">правило пользования сервисом</a></label>
                          </div>
                          <div class="form-group form-button">
                              <input type="submit" name="btn_submit_register" id="signup" class="form-submit" value="Зарегистрироваться" />
                          </div>
                      </form>
                  </div>
                  <div class="signup-image">
                      <figure><img src="images/signup-image.jpg" alt="sing up image"></figure>
                      <a href="form_auth.php" class="signup-image-link">Я уже зарегистрирован</a>
                  </div>
              </div>
          </div>
      </section>
  <?php
    } else {
    ?>
      <div id="authorized">
          <h2>Вы уже зарегистрированы</h2>
      </div>
  <?php
    }
    ?>

<script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main.js"></script>