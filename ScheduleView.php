<?php 
                      $city = $_SESSION['email'];

                      if ($stmt = $mysqli->prepare("SELECT last_name FROM `users` WHERE email=?")) {
                        
                        /* связываем параметры с метками */
                        $stmt->bind_param("s", $city);
                    
                        /* запускаем запрос */
                        $stmt->execute();
                    
                        /* связываем переменные с результатами запроса */
                        $stmt->bind_result($last_name);
                    
                        /* получаем значения */
                        $stmt->fetch();
                    
                        /* закрываем запрос */
                        $stmt->close();
                    }
                      /* создаем подготавливаемый запрос */
                      if ($stmt = $mysqli->prepare("SELECT name FROM `users` WHERE email=?")) {
                        
                          /* связываем параметры с метками */
                          $stmt->bind_param("s", $city);
                      
                          /* запускаем запрос */
                          $stmt->execute();
                      
                          /* связываем переменные с результатами запроса */
                          $stmt->bind_result($district);
                      
                          /* получаем значения */
                          $stmt->fetch();
                      
                          printf("%s %s\n", $district,$last_name);
                      
                          /* закрываем запрос */
                          $stmt->close();
                      }
                    
                      /* закрываем соединение */
                      $mysqli->close();
                      ?>