<?php
//Запускаем сессию
session_start();
?>
<?php
//Проверяем авторизован ли пользователь
if (!isset($_SESSION['email']) && !isset($_SESSION['password'])) {
  // если нет, то выводим блок с ссылками на страницу регистрации и авторизации
  header('Location: http://tesst/form_auth.php ');
  exit;
?>
<?php
} else {
  //Если пользователь авто    ризован, то выводим ссылку Выход
?>
<?php
//Подключение шапки
require("dbconnect.php");
?>

<!DOCTYPE html>
<html lang="en">


<head>

  <meta charset="utf-8">
  <title>Название нашего сайта</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="css/styles.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="assets/images/favicon.png" />
  <script type="text/javascript">
    $(document).ready(function() {
      "use strict";
      //================ Проверка email ==================

      //регулярное выражение для проверки email
      var pattern = /^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i;
      var mail = $('input[name=email]');

      mail.blur(function() {
        if (mail.val() != '') {

          // Проверяем, если введенный email соответствует регулярному выражению
          if (mail.val().search(pattern) == 0) {
            // Убираем сообщение об ошибке
            $('#valid_email_message').text('');

            //Активируем кнопку отправки
            $('input[type=submit]').attr('disabled', false);
          } else {
            //Выводим сообщение об ошибке
            $('#valid_email_message').text('Не правильный Email');

            // Дезактивируем кнопку отправки
            $('input[type=submit]').attr('disabled', true);
          }
        } else {
          $('#valid_email_message').text('Введите Ваш email');
        }
      });

      //================ Проверка длины пароля ==================
      var password = $('input[name=password]');

      password.blur(function() {
        if (password.val() != '') {

          //Если длина введенного пароля меньше шести символов, то выводим сообщение об ошибке
          if (password.val().length < 6) {
            //Выводим сообщение об ошибке
            $('#valid_password_message').text('Минимальная длина пароля 6 символов');

            // Дезактивируем кнопку отправки
            $('input[type=submit]').attr('disabled', true);

          } else {
            // Убираем сообщение об ошибке
            $('#valid_password_message').text('');

            //Активируем кнопку отправки
            $('input[type=submit]').attr('disabled', false);
          }
        } else {
          $('#valid_password_message').text('Введите пароль');
        }
      });
    });
  </script>
</head>

<body>
  <div class="container-scroller">
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="assets/images/logo.svg" alt="logo" /></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="assets/images/logo-mini.svg" alt="logo" /></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <div class="nav-profile-img">
                <img src="assets/images/faces/face1.jpg" alt="image">
                <span class="availability-status online"></span>
              </div>
              <div class="nav-profile-text">
                <p class="mb-1 text-black">
                  <?php
                  $city = $_SESSION['email'];

                  if ($stmt = $mysqli->prepare("SELECT last_name FROM `users` WHERE email=?")) {

                    /* связываем параметры с метками */
                    $stmt->bind_param("s", $city);

                    /* запускаем запрос */
                    $stmt->execute();

                    /* связываем переменные с результатами запроса */
                    $stmt->bind_result($last_name);

                    /* получаем значения */
                    $stmt->fetch();

                    /* закрываем запрос */
                    $stmt->close();
                  }
                  /* создаем подготавливаемый запрос */
                  if ($stmt = $mysqli->prepare("SELECT name FROM `users` WHERE email=?")) {

                    /* связываем параметры с метками */
                    $stmt->bind_param("s", $city);

                    /* запускаем запрос */
                    $stmt->execute();

                    /* связываем переменные с результатами запроса */
                    $stmt->bind_result($district);

                    /* получаем значения */
                    $stmt->fetch();

                    printf("%s %s\n", $district, $last_name);

                    /* закрываем запрос */
                    $stmt->close();
                  }

                  /* закрываем соединение */

                  ?>
                </p>
              </div>
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item" href="#">
                <i class="mdi mdi-cached mr-2 text-success"></i> Activity Log </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" name="exit" href="/logout.php">
                <i class="mdi mdi-logout mr-2 text-primary"></i> Signout </a>
              <?php
              if (isset($_POST['exit'])) {
                $_SESSION = array();

                // сбросить куки, к которой привязана сессия
                if (ini_get("session.use_cookies")) {
                  $params = session_get_cookie_params();
                  setcookie(
                    session_name(),
                    '',
                    time() - 42000,
                    $params["path"],
                    $params["domain"],
                    $params["secure"],
                    $params["httponly"]
                  );
                }

                // уничтожить сессию
                session_destroy();
              }


              ?>
            </div>
          </li>
          <li class="nav-item d-none d-lg-block full-screen-link">
            <a class="nav-link">
              <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="mdi mdi-email-outline"></i>
              <span class="count-symbol bg-warning"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <h6 class="p-3 mb-0">Сообщения</h6>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="assets/images/faces/face4.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                  <h6 class="preview-subject ellipsis mb-1 font-weight-normal">Mark send you a message</h6>
                  <p class="text-gray mb-0"> 1 минуту назад </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="assets/images/faces/face2.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                  <h6 class="preview-subject ellipsis mb-1 font-weight-normal">Cregh send you a message</h6>
                  <p class="text-gray mb-0"> 15 Minutes ago </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <img src="assets/images/faces/face3.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                  <h6 class="preview-subject ellipsis mb-1 font-weight-normal">Profile picture updated</h6>
                  <p class="text-gray mb-0"> 18 Minutes ago </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <h6 class="p-3 mb-0 text-center">4 new messages</h6>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell-outline"></i>
              <span class="count-symbol bg-danger"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <h6 class="p-3 mb-0">Уведомления</h6>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="mdi mdi-calendar"></i>
                  </div>
                </div>
                <div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                  <h6 class="preview-subject font-weight-normal mb-1">Event today</h6>
                  <p class="text-gray ellipsis mb-0"> Just a reminder that you have an event today </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="mdi mdi-settings"></i>
                  </div>
                </div>
                <div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                  <h6 class="preview-subject font-weight-normal mb-1">Settings</h6>
                  <p class="text-gray ellipsis mb-0"> Update dashboard </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="mdi mdi-link-variant"></i>
                  </div>
                </div>
                <div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                  <h6 class="preview-subject font-weight-normal mb-1">Launch Admin</h6>
                  <p class="text-gray ellipsis mb-0"> New admin wow! </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <h6 class="p-3 mb-0 text-center">See all notifications</h6>
            </div>
          </li>
          <li class="nav-item nav-logout d-none d-lg-block">
            <a class="nav-link" name="exit" href="/logout.php">
              <i class="mdi mdi-power"></i>
            </a>
          </li>
          <li class="nav-item nav-settings d-none d-lg-block">
            <a class="nav-link" href="#">
              <i class="mdi mdi-format-line-spacing"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">
                <img src="assets/images/faces/face1.jpg" alt="profile">
                <span class="login-status online"></span>
                <!--change to offline or busy as needed-->
              </div>
              <div class="nav-profile-text d-flex flex-column">
                <span class="font-weight-bold mb-2">
                  <?php
                  printf("%s %s\n", $district, $last_name);
                  ?>
                </span>
                <span class="text-secondary text-small">

                  <?php
                  $email = $_SESSION['email'];
                  /* создаем подготавливаемый запрос */
                  if ($stmt = $mysqli->prepare("SELECT `group` FROM `users` WHERE email=?")) {

                    /* связываем параметры с метками */
                    $stmt->bind_param("s", $email);

                    /* запускаем запрос */
                    $stmt->execute();

                    /* связываем переменные с результатами запроса */
                    $stmt->bind_result($district);

                    /* получаем значения */
                    $stmt->fetch();

                    printf("Группа: %s\n", $district);

                    /* закрываем запрос */
                    $stmt->close();
                  }


                  ?>
                </span>
              </div>
              <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <span class="menu-title">Новости</span>
              <i class="mdi mdi-home menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="estimations.php">
              <span class="menu-title">Оценки</span>
              <i class="mdi  mdi-crosshairs-gps menu-icon"></i>
            </a>
          </li>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="NewScgedule.php">
              <span class="menu-title">Расписание</span>
              <i class="mdi mdi-contacts menu-icon"></i>
            </a>
          </li>
          </li>
          <li class="nav-item sidebar-actions">
            <span class="nav-link">
            </span>
          </li>
        </ul>
      </nav>
      <div class="main-panel">
        <div class="content-wrapper">


          <script src="assets/vendors/js/vendor.bundle.base.js"></script>
          <!-- endinject -->
          <!-- Plugin js for this page -->
          <script src="assets/vendors/chart.js/Chart.min.js"></script>
          <!-- End plugin js for this page -->
          <!-- inject:js -->
          <script src="assets/js/off-canvas.js"></script>
          <script src="assets/js/hoverable-collapse.js"></script>
          <script src="assets/js/misc.js"></script>
          <!-- endinject -->
          <!-- Custom js for this page -->
          <script src="assets/js/dashboard.js"></script>
          <script src="assets/js/todolist.js"></script>
          <!-- End custom js for this page -->
          <?php

}
?>